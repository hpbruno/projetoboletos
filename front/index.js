const express = require('express')
const app = express()
const bodyparser = require('body-parser')
const axios = require('axios')

app.use(express.static('./src'))
app.use(express.static('./src/assets'))

app.use(bodyparser.urlencoded({extended: true})); 

app.get('/', ( req, res )  => {    
  res.sendFile('/login.html', { root:__dirname + '/src/components/login/'}) 
})

app.get('/home', ( req, res ) => {              
   res.sendFile('/home.html', { root:__dirname + '/src/components/home/'})  
}) 

app.listen(3312, () => { console.log('Executando front em 3312') })   