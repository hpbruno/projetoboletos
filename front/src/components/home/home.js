/*
 - @Author: Bruno Henrique
*/

function retornartoken() {

    let link = location.href
    if (link.indexOf('token') > 0) {
        let token = link.substring(link.indexOf('=') +1)         
        return token
    }
}

function carregarcampos() {
    
    // Sempre que carregar a solicitacao, vamos limpara os avisos.
    limparalertas()

    let solicitacao = {            
            
        banco:            document.getElementById('banco').value,
        conta:            document.getElementById('conta').value, 
        dvconta:          document.getElementById('dvconta').value,
        agencia:          document.getElementById('agencia').value,
        dvagencia:        document.getElementById('dvagencia').value, 
        carteira:         retornarvalorcombo('carteira'),
        cedente:          document.getElementById('cedente').value,
        convenio:         document.getElementById('convenio').value,
        cnab:             retornarvalorcombo('cnab'),
        tipoemissao:      retornarvalorcombo('tipoemissao'),
        tipojuros:        retornarvalorcombo('juros'),
        valorjuros:       document.getElementById('valorjuros').value, 
        tipomulta:        retornarvalorcombo('multa'),
        valormulta:       document.getElementById('valormulta').value, 
        tipoprotesto:     retornarvalorcombo('protesto'),
        diasprotesto:     document.getElementById('diasprotesto').value, 
        considerataxa:    document.getElementById('considerataxa').checked,  
        nnini:            document.getElementById('nninicial').value,  
        nncorrente:       document.getElementById('nncorrente').value,  
        nnfinal:          document.getElementById('nnfinal').value,              
        instrucaoboleto:  document.getElementById('instrucaoboleto').value,  
        observacao:       document.getElementById('observacao').value  
        
    }

    if (enviarsolicitacao(solicitacao)){
        exibiralert('Solicitação enviada com sucesso!', 'OK')
    }

}

function enviarsolicitacao(solicitacao) {

    axios.post('http://localhost:4000/validation', solicitacao)
         .then( res => {
            // Exibir alert 
            exibiralert(res.data.msg, res.data.tipo)

            if (res.data.tipo === 'err') {
                configurarcampoerro(res.data.campo)    
            }
            
         })

}

function configurarcampoerro(campo) {


    let field = document.getElementById(campo)

    field.setAttribute('style','border: 2px solid red; border-radius: 3px')

    field.onfocus = () => {
        field.setAttribute('style','border: 0.5px inset; border-radius: 3px;')
    }
}

function exibiralert(conteudo, tipo) {
   
    // Informar Alert 
    const alert = document.getElementById('alert')
    alert.setAttribute('style', 'transition: 1.5s; opacity: 1;')    

    alert.innerHTML = ''

    // close do alert 
    const close = createElements('div', 'close', 'class', 'close', '', 'X')    
    close.onclick = click => alert.style.opacity = 0
    alert.appendChild(close)

    const content = createElements('h6','alertcontent', 'class', 'alertcontent', '', conteudo)    
    alert.appendChild(content)

    if (tipo === 'OK') {
        alert.style.backgroundColor = '#70B5C0'        
    } else if (tipo === 'ERR') {
        alert.style.backgroundColor = 'rgb(84, 32, 32)'                
    }
}

function carregarBancos() {

    axios.get('http://localhost:4000/bank')
    .then(res => {

        res.data.forEach(bancos => {
            // parametros -> nome da tag, id da tag pai, conteudo da tag
            createElements('option', 'bankitem', 'value', bancos.ID, 'banco', bancos.NUMEROBANCO +' - '+ bancos.NOME)
        });       

        // Vamos criar uma option de instucao. 
        createElements('option', 'bankitem', 'disabled', 'true', 'banco', 'Selecione o banco')

    })
}

function carregarCarteiras() {

    // Recuperar banco selecionado 
    let banco = document.getElementById('banco')    

    // sempre que recarregar as carteiras, vamos limpar as fields. 
    limparContainerFields('containerCarteira');

    if (banco.selectedIndex > -1) {

        let body = {
            bank: banco.options[banco.selectedIndex].value
        }
    
        axios.post('http://localhost:4000/carteiras', body)
        .then(res => {
    
            res.data.forEach(carteira => {            
                createElements('option', 'carteiraitem', 'value', carteira.ID, 'carteira', carteira.DESCRICAO)
            })
        })
    }    
}

function createElements(tag, idtag, attribute, attributevalue,  idtagPai, content) {

    // create
    let element = document.createElement(tag)            
    
    // id da tag
    element.setAttribute('id', idtag)   

    // attributes - Se o atrubuto vier disable, é porque é uma oppcao instrutiva     
    element.setAttribute(attribute, attributevalue)
    if (attribute == 'disabled') element.setAttribute('selected', 'true')    

    // conteudo da tag 
    element.innerHTML = content

    // posicionar elemento se existir 
    const ele = document.getElementById(idtagPai)
    if (ele) ele.appendChild(element)  
    
    return element
}   

function limparalertas() {
    let container = document.getElementById('alert')    
    let childs = container.querySelectorAll('div')

    if (childs.length > 0) {
        childs.forEach(child => {
            child.outerHTML = ''
        })
    }
}

function retornarvalorcombo(idselect) {

    let element = document.getElementById(idselect)
    
    if (element.selectedIndex > -1) {
        return element.options[element.selectedIndex].value
    } else {
        return 0
    }

}

function limparContainerFields(idcontainer) {  
    
    // Recuperar container
    let container = document.getElementById(idcontainer)

    // Limpar os inputs 
    let inputs = container.querySelectorAll('input')

    inputs.forEach(inp => {

        // Type text 
        if (inp.Type = 'text') inp.value = ''
    })

    // Limpar Selects 
    let selects = container.querySelectorAll('select') 

    selects.forEach(select => {
        
        if (select.id === 'carteira') {
            let options = select.querySelectorAll('option')

            options.forEach(op => {            
                op.outerHTML = ''                
            })
        }
    })

}

function carrgearCombos() {
        
        /** -- Nao mudar essa ordem, precisamos carregar os bancos para definir as carteiras -- **/ 
        carregarBancos()     
        carregarCarteiras()
        /* ================= */

        // ** Aqui vamos carregar os demais campos. -> Encapsular em metodos conforme necessidade         

        // Combo: CNAB
        createElements('option', 'cnab', 'value', '240', 'cnab', 'CNAB240')
        createElements('option', 'cnab', 'value', '400', 'cnab', 'CNAB400')

        // Combo: tipo emissão
        createElements('option', 'tipoemissaoitem', 'disabled', 'true',    'tipoemissao', 'Informe o responsavel pela emissão dos boletos')
        createElements('option', 'tipoemissaoitem', 'value',    'TE_EMP',  'tipoemissao', 'A emissão dos boletos será de responsabilidade da empresa')
        createElements('option', 'tipoemissaoitem', 'value',    'TE_BANK', 'tipoemissao', 'A emissão dos boletos será de responsabilidade do banco')
        
        // Combo: Tipo Juros
        createElements('option', 'tipojuros', 'value', 'TJ_MENSAL', 'juros', 'Mensal')
        createElements('option', 'tipojuros', 'value', 'TJ_DIARIO', 'juros', 'Diário')
        createElements('option', 'tipojuros', 'value', 'TJ_FIXO',   'juros', 'Valor Fixo')

        // Combo: Tipo Multa
        createElements('option', 'tipomulta', 'value', 'TM_MENSAL', 'multa', 'Mensal')
        createElements('option', 'tipomulta', 'value', 'TM_DIARIO', 'multa', 'Diário')
        createElements('option', 'tipomulta', 'value', 'TM_FIXO',   'multa', 'Valor Fixo')

        // Combo: Protesto 
        createElements('option', 'tipoprotesto', 'value', 'TP_NAOPROTESTAR', 'protesto', 'Não protestar')
        createElements('option', 'tipoprotesto', 'value', 'TP_DIASUTEIS',    'protesto', 'Dias Uteis')
        createElements('option', 'tipoprotesto', 'value', 'TP_DIASCORRIDOS', 'protesto', 'Dias Corridos')
}

function carregarEventos() {

    /** BANCOS **/
    const bankSelect = document.getElementById('banco')
    bankSelect.onchange = () => { carregarCarteiras() }

    /** SUBMIT **/ 
    const btnSend = document.getElementById('btnSend')
    btnSend.onclick = () => { carregarcampos() }
}

function start() {
    // Carregar o token de acesso antes de tudo
    axios.defaults.headers.common['Authorization'] = `bearer ${retornartoken()}`

    carrgearCombos()
    carregarEventos()    

    carregarEventos()
}

start()


