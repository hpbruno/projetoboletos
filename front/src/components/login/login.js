/*
 - @Author: Bruno Henrique
*/

function isValid( token ) {
        
    if (!token) {
        return false
    } else {
        return true
    }        
}


function registrarToken() {

    if (isValid(empid.value)) {                
        login = {
            empid: empid.value            
        }    
    }

    axios.post('http://localhost:4000/geratoken', login)
        .then( res => {
            console.log(res)
        })
}

function colar () {
    navigator.clipboard.readText().then(text => {
        document.getElementById('empid').value = text
    })
}

function validarAcesso() {

    let empid = document.getElementById('empid')
    if(isValid(empid.value)) {
                
        login = { empid: empid.value }          
    }

    axios.post('http://localhost:4000/auth ', login)
    .then(  res => {
        
        
        if ((res.status === 200)) {
            window.location.href = `http://localhost:3312/home?token=${res.data.token}`
        } else if((res.status === 201)){                        
            InformarErro(res.data.msg)
        }        
    })      
}

function InformarErro(msg) {

    const campo = document.getElementById('empid') 

    // Setar erro no campo empid 
    campo.setAttribute('style','border: 2px solid red; border-radius: 3px')
    campo.onfocus = () => {
        campo.setAttribute('style','border: 0.5px inset; border-radius: 3px;')
    }

    // Informar Alert 
    const alert = document.getElementById('alert')
    alert.setAttribute('style', 'transition: 1.5s; opacity: 1;')    

    alert.innerHTML = ''

    // close do alert 
    const close = createElements('div', 'close')
    close.innerHTML = 'X'
    close.onclick = click => alert.style.opacity = 0
    alert.appendChild(close)

    const content = createElements('h2', '')
    content.innerHTML = msg 
    alert.appendChild(content)

}

function carregareventos() {

    const btnAuth = document.getElementById('btnauth')
    const btnGerarToken = document.getElementById('btngerartoken')
    
    // gerar token
    if (btnGerarToken) {
        btnGerarToken.onclick = clic => {
            registrarToken()
        }
    }

    // validar acesso
    if (btnAuth) {
        btnAuth.onclick = clic => {      
            validarAcesso()
        }
    }
} 

function createElements(tag, className) {
    
    const element = document.createElement(tag) 
    if (className != '') {
        element.className = className;
    }

    return element
} 

function start() {
    carregareventos()    
    isValid()    
    
} 

start()