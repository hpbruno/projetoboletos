/*
 - @Author: Bruno Henrique
*/

const nodemailer = require('nodemailer')

module.exports = app => {

    const validarsolicitacao = ( req, res ) => {
        
        let banco = {

            banco: req.body.banco, 
            conta: req.body.conta,  
            dvconta: req.body.dvconta, 
            agencia: req.body.agencia, 
            dvagencia: req.body.dvagencia 
        }

        let carteira = { 

            carteira:        req.body.carteira,
            cedente:         req.body.cedente, 
            convenio:        req.body.convenio, 
            cnab:            req.body.cnab, 
            tipoemissao:     req.body.tipoemissao,
            tipojuros:       req.body.tipojuros,
            valorjuros:      req.body.valorjuros,
            tipomulta:       req.body.tipomulta,
            valormulta:      req.body.valormulta,
            tipoprotesto:    req.body.tipoprotesto,
            diasprotesto:    req.body.diasprotesto,
            considerataxa:   req.body.considerataxa,
            nnini:           req.body.nnini,
            nncorrente:      req.body.nncorrente,
            nnfinal:         req.body.nnfinal,
            instrucaoboleto: req.body.instrucaoboleto,
            observacao:      req.body.observacao
        } 

        // vamos verificar os campos
        let verifica = verificarcamposobrigatorios(banco, carteira)

        // Apos a verificacao validar, vamos tentar registrar
        if ( verifica.tipo == 'OK' ) {
        
            if (registrarSolicitacao( banco, carteira )) {
               res.status(200).send( { msg: 'Sua solicitação foi enviada com sucesso!', tipo:'OK' } )
            }
        } else {            
            // Existe um campo em verifica que infoma o tipo de retorno,  err ou ok, no front tratamos como isso aparce ao ususario 
            res.status(200).send(verifica)          
        }        
    }

    const verificarcamposobrigatorios = (banco, carteira) => {
        
        // ** Validar campos obrigratorios **//
        
        console.log(  );

        // Banco
        if ( banco.banco == 'Selecione o banco' ) {
            return  { campo:'banco', msg:'Favor verificar banco informado!', tipo:'err' }
        }

        // Conta bancária
        if ( isNaN(parseInt(banco.conta)) )  {
            return  { campo:'conta', msg:'Favor verificar conta bancária!', tipo:'err' }
        }  
        if (( banco.dvconta == '' ) || (banco.dvconta.length > 2 )) {
            return  { campo:'dvconta',msg:'Favor verificar o digito da conta!', tipo:'err' }
        }

        // Agencia
        if ( isNaN(parseInt(banco.agencia)) ) {
            return  { campo:'agencia',msg:'Favor verificar agencia!', tipo:'err' }
        }
        
        if ( (banco.dvagencia == '') || (banco.dvagencia.length > 2 ) ) {
            return  { campo:'dvagencia', msg:'Favor verificar o digito da agencia!', tipo:'err' }
        }


        // validar carteira
        if ( carteira.carteira == '' )       return  { campo:'carteira'       , msg:'Favor verificar a carteira informada!'                , tipo:'err' }        
        if ( carteira.cedente == '' )         return  { campo:'cedente'        , msg:'Favor verificar o cedente informado!'                 , tipo:'err' }
        if ( carteira.convenio == '')        return  { campo:'convenio'       , msg:'Favor verificar o convênio informado!'                , tipo:'err' }
        if ( carteira.cnab == '')            return  { campo:'cnab'           , msg:'Favor verificar o CNAB informado!'                    , tipo:'err'}
        if ( carteira.tipoemissao < 0)       return  { campo:'tipoemissao'    , msg:'Favor verificar o tipo de emissão informado!'         , tipo:'err'}
        if ( carteira.tipojuros < 0)         return  { campo:'tipojuros'      , msg:'Favor verificar verificar o tipo de juros informado!' , tipo:'err'}
        if ( carteira.valorjuros == '')      return  { campo:'valorjuros'     , msg:'Favor verificar verificar o valor de juros!'          , tipo:'err' }
        if ( carteira.tipomulta < 0 )        return  { campo:'tipomulta'      , msg:'Favor verificar o tipo de multa informado!'           , tipo:'err'}
        if ( carteira.valormulta == '')      return  { campo:'valormulta'     , msg:'Favor verificar o valor de multa informado!'          , tipo:'err'}
        if ( carteira.tipoprotesto < 0)      return  { campo:'tipoprotesto'   , msg:'Favor verificar o tipo de protesto informado!'        , tipo:'err'}
        if ( carteira.diasprotesto == '')    return  { campo:'diasprotesto'   , msg:'Favor verificar os dias de protesto informado!'       , tipo:'err'}       
        if ( carteira.nnini == '')           return  { campo:'nnini'          , msg:'Favor verificar a posicao inicial do nosso numero!'   , tipo:'err'}
        if ( carteira.nncorrente == '')      return  { campo:'nncorrente'     , msg:'Favor verificar a posicao corrente do nosso numero!'  , tipo:'err' }
        if ( carteira.nnfinal == '')         return  { campo:'nnfinal'        , msg:'Favor verificar a posicao final do nosso numero!'     , tipo:'err'}
         if ( carteira.instrucaoboleto == '') return  { campo:'instrucaoboleto', msg:'Favor verificar as instruções do boleto informados!'  , tipo:'err'}

        return { campo: '', msg: 'Dados validados', tipo:'OK' }  
    }   

    const registrarSolicitacao = (banco, carteira) => {

        try {
            // connection 
            const con = app.config.db.connection

            // INSERT 
            const sql = 'INSERT INTO solicitacoes' + 
                        '   ( IDCLIENTE, IDBANCO, IDCARTEIRA, NUMEROAGENCIA, DIGITOAGENCIA, NUMEROCONTA, DIGITOCONTA, TIPOJUROS, VALORJUROS, ' + 
                        '     TIPOMULTA, VALORMULTA, TIPOPROTESTO, DIASPROTESTO, CONVENIO, CEDENTE, CNAB, CONSIDERARTAXA, NOSSONUMEROINI, NOSSONUMEROCOR, NOSSONUMEROFIM, VARIACAO, MODALIDADE )' +
                        '  VALUES ?;'
            
            // values                    
            let values = [
                [1, banco.banco, carteira.carteira, banco.agencia, banco.dvagencia, banco.conta, banco.dvconta, carteira.tipojuros, carteira.valorjuros,
                 carteira.tipomulta, carteira.valormulta, carteira.tipoprotesto, carteira.diasprotesto, carteira.convenio, carteira.cedente, carteira.cnab, 1,  // Ajustar o considerar taxa que eu tratei com o booleab la fora, mas aqui tem que vir inteiro
                 carteira.nnini, carteira.nncorrente, carteira.nnfinal, 0, '']            
            ]                   
            
            con.query(sql, [values], (err, result, fields) => {
                if (err) console.log(err);                

                let solicitacao = {
                    banco, 
                    carteira
                }

                // Email
                enviarsolicitacaoemail(solicitacao)
            })

            return true
        } catch (err) {
            return false
        }
    }

    const retornarsolicitacoes = async (req, res) => {

        try {

            const con = app.config.db.connection
            const sql = 'SELECT * FROM solicitacoes;'

            await con.query(sql, (err, result, fields) => {
                if (err) console.log('Err:'+err)
                res.status(200).send(XML(result))
                console.log(result);
            })
                        

        } catch (err){ 
            
        }

    }

    const enviarsolicitacaoemail = (solicitacao) => {
        let nomebanco
        const con = app.config.db.connection 
        const sql = 'SELECT * FROM bancos WHERE ID = ?;'

        // Dados da solicitacao
        let idbanco = solicitacao.banco.banco
        let cedente = solicitacao.carteira.cedente
                
        //recuperar o nome do banco --> isolar em um metodo
        con.query(sql, [idbanco], function(err, result, fields) {
            if (err) return console.log(err)
            result.forEach(bank => {                
                nomebanco = bank.NUMEROBANCO + ' - ' + bank.NOME                                       
            })              
            
            // from
            let from = nodemailer.createTransport({
                host: "smtp.gmail.com",
                port: 587, 
                service:"gmail",
                secure: true, 
                auth:{
                    user: "sounrunoh@gmail.com", 
                    pass: "mostardabesta" // Criptografar
                }
            })
            
            
            let to = {
                from:"sounrunoh@gmail.com", 
                to: "hpbruno1@outlook.com", 
                subject: "Homologacao de boletos - " +  nomebanco, 
                text: 'Segue dados da solicitacao : \n \n'  +
                      'Banco: '                    + nomebanco +                                                        '\n' +
                      'Agência: '                  + solicitacao.banco.agencia + '-' + solicitacao.banco.dvagencia + '\n' +
                      'Conta Bancária: '           + solicitacao.banco.conta + '-' + solicitacao.banco.dvconta +        '\n' +
                      ' \n' +
                      'Carteira: '                 + solicitacao.carteira.carteira + '\n' +
                      'Cdente: '                   + solicitacao.carteira.cedente + '\n' +
                      'Convênio: '                 + solicitacao.carteira.convenio + '\n' +
                      'Layout: '                   + solicitacao.carteira.cnab + '\n' +
                      'Emissor: '                  + solicitacao.carteira.tipoemissao + '\n' +
                      ' \n' +
                      'Tipo de Multa: '            + solicitacao.carteira.tipomulta + '\n' +
                      'Valor Multa: '              + solicitacao.carteira.valormulta + '\n' +
                      'Tipo Juros: '               + solicitacao.carteira.tipojuros + '\n' +
                      'Valor Juros: '              + solicitacao.carteira.valorjuros + '\n' +
                      'Tipo Protesto: '            + solicitacao.carteira.tipoprotesto + '\n' +
                      'Dias Protesto: '            + solicitacao.carteira.diasprotesto +'\n' +
                      'Nosso Numero (Inicial):'    + solicitacao.carteira.nnini + '\n' +
                      'Nosso Numero (Corrente):'   + solicitacao.carteira.nncorrente + '\n' +
                      'Nosso Numero (Final):'      + solicitacao.carteira.nnfinal + '\n' +
                      'Instruções do boleto: '     + solicitacao.carteira.instrucaoboleto +'\n' +
                      'Observações do cliente: '   + solicitacao.carteira.observacao                
            }

            from.sendMail(to, function(err){
                if (err) console.log(err)                
            })
        
        })         
    }

    const verificarinteiro = (campo) => {

        campo.isInteger

    }

    return { validarsolicitacao, retornarsolicitacoes }
}