const { authSecret } = require('../.env')
const jwt = require('jwt-simple')
const bcrypt = require('bcrypt-nodejs')

module.exports = app => {

    // validar acesso 1
    const validate = async ( req, res ) => {
        let empid = req.body.empid

        const con = app.config.db.connection
        const sql = 'SELECT * FROM _auth WHERE CODIGOCLIENTE = ?;'
        
        await con.query(sql, [empid], function(err, result, fields) {
            if (err) return console.log(err)
            
            // Convertendo para segundos
            const now = Math.floor(new Date() / 1000 ) 

            const client = result
            if (client.length > 0) {
                client.forEach(cli => {
                    
                    const payload = {
                        ID: cli.ID, 
                        CODIGOCLIENTE: cli.CODIGOCLIENTE, 
                        TOKEN: cli.TOKEN,  
                        IAT: cli.DATAGERACAO, 
                        EXP: cli.DATAEXPIRACAO
                    }
                    
                    validateToken({ ...payload, token:jwt.encode(payload, authSecret)})
                    .then(valid => {
                        try {
                            // se o token ainda for valido, vamos liberar a entrada 
                            if (valid) {
                                res.status(200).json({
                                    ...payload, 
                                    token: jwt.encode(payload, authSecret)
                                })
                            } else {
                                res.status(201).send( { msg:'Desculpe, seu token expirou, entre em contato com o suporte!'}) 
                            }
                            
                        } catch (err) {
                        res.status(500).send(err) // Erro no servidor 
                        }
                    })
                })
            } else {                             
                res.status(201).send({ msg:'Desculpe, não encontei o código informado!'})
            }

        })  
    }    

    const validateToken = async ( clientData ) => {
        let isvalid

        try{
            const token = jwt.decode(clientData.token, authSecret)
            
            // vamos verificar a data de expiracao             
            console.log(new Date())   
            console.log(new Date(token.EXP * 1000));         
            
            if (new Date(token.EXP * 1000) > new Date()) {
                isvalid = true
            } else {
                isvalid = false
            }

        } catch(err) {
            isvalid = false
        }   
        
        return isvalid
    }


    // Ecript 
    const encript = token => {
        
        const salt = bcrypt.genSalt(10, function (err) {
            if (err) {
                console.log('error');
            }            
        })

        return bcrypt.hashSync(token, salt)            
    }

    // retornar auth
    const gerarAuth = async ( req, res ) => {
        
        const con = app.config.db.connection        

        // Convertendo para segundos
        const now = Math.floor(new Date() / 1000 ) 

        
        let empid = req.body.empid
        let token = encript(req.body.empid)        
        let iat   = now
        let exp   = now  + (60 * 60 * 24 * 3) // 03 dias 

        const values = [
                [empid, token, iat, exp]
        ]

        const sql = 'INSERT INTO _auth' +
                    '   (CODIGOCLIENTE, TOKEN, DATAGERACAO, DATAEXPIRACAO)' + 
                    'VALUES ?' ;                    
    
        
        await con.query(sql, [values], function(err, results) {
            if (err) return res.status(401).send(err)            
            res.status(200).send('Token gerado com sucesso: '+token);
        })        
    }

    return { validate, gerarAuth, validateToken }
}