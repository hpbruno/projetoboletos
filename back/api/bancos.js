/*
 - @Author: Bruno Henrique
 */

 module.exports = app => {

    const retornarBancos = (req, res) => {

        const con = app.config.db.connection 
        const sql = 'SELECT * FROM bancos;'

        con.query(sql, function(err, result, fields) {

            if (err) res.status(500).send(err)
            res.status(200).send(result)                        
        })        
    }


    const retonarCarteirasBanco = (req, res) => {

        const con = app.config.db.connection
        const sql = 'SELECT * FROM bancos B ' + 
                    'INNER JOIN carteiras C ON'+
                    '   B.ID = C.IDBANCO ' +
                    'WHERE 1=1' + 
                    '   AND C.IDBANCO = ?;';

        con.query(sql, [req.body.bank], (err, result, fields) => {
            if (err) res.status(500).send(err)
            res.status(200).send(result)
        })

    }

    return { retornarBancos, retonarCarteirasBanco }
 }
