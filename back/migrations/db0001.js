

module.exports = app => {
    
    /* CREATES */

    const createDatabase = (con) => {
        const sql = 'CREATE DATABASE IF NOT EXISTS cobranca;'
                    

        con.query(sql, (err, result, fields) => {
            if (err) return console.log(err)
            console.log('Database: cobranca')
        })
    }

    const setDatabase = (con) => {
        const sql = 'USE cobranca;'

        con.query(sql, (err, result, fields) => {
            if (err) return console.log(err)
            console.log('Base selecionada: cobranca')
        })
    }

    const createauth = (con) => {
        
        const sql = ' CREATE TABLE IF NOT EXISTS  _auth (' +
                    '   ID INT AUTO_INCREMENT PRIMARY KEY ,' + 
                    '   CODIGOCLIENTE INT,'+
                    '   TOKEN varchar(100),'+
                    '   DATAGERACAO BIGINT NOT NULL,'+
                    '   DATAEXPIRACAO BIGINT NOT NULL );'

        con.query(sql, function (err, results, fields) {
            if (err) return console.log(err)
            console.log('table: _auth')
        })
    }
        
    const createbancos = (con) => {
        
        const sql = 'CREATE TABLE IF NOT EXISTS bancos ('+
                    '   ID INT AUTO_INCREMENT PRIMARY KEY,' + 
                    '   NOME VARCHAR(50),' + 
                    '   NUMEROBANCO VARCHAR(3),' +  
                    '   LOCALPAGAMENTO VARCHAR(150),'+  
                    '   NOMECONTATO VARCHAR(50),' + 
                    '   EMAILCONTATO VARCHAR(50) );'

        con.query(sql, (err, result, fields) => {
            if(err) return console.log(err);
            console.log('table: bancos')
        })

    }

    const createcarteiras = (con) => {
        
        const sql = 'CREATE TABLE IF NOT EXISTS carteiras (' + 
                    '   ID INT AUTO_INCREMENT PRIMARY KEY,' + 
                    '   DESCRICAO VARCHAR(250),' + 
                    '   TIPO VARCHAR(50),' + 
                    '   IDBANCO INT, ' +
                    'FOREIGN KEY(IDBANCO) REFERENCES bancos(ID) );' 

        con.query(sql, (err, result, fields) => {
            if (err) return console.log(err) 
            console.log('table: carteiras')
        })
    }

    const createsolicitacoes = (con) => {
        
        const sql = 'CREATE TABLE IF NOT EXISTS solicitacoes (' +
                    '   ID             INT AUTO_INCREMENT PRIMARY KEY, ' +
                    '   IDCLIENTE      INT, ' +
                    '   IDBANCO        INT, ' +
                    '   IDCARTEIRA     INT, ' +
                    '   NUMEROAGENCIA  BIGINT, ' +
                    '   DIGITOAGENCIA  VARCHAR(2),' +
                    '   NUMEROCONTA    BIGINT,' +
                    '   DIGITOCONTA    VARCHAR(2), '+ 
                    '   TIPOJUROS      VARCHAR(50),' +                    
                    '   VALORJUROS     DOUBLE,' +
                    '   TIPOMULTA      VARCHAR(50),'+
                    '   VALORMULTA     DOUBLE,' +
                    '   TIPOPROTESTO   VARCHAR(50),' +
                    '   DIASPROTESTO   INT,' + 
                    '   CONVENIO       BIGINT,' +
                    '   CEDENTE        BIGINT,' +                    
                    '   CNAB           INT,' +
                    '   CONSIDERARTAXA INT,' +
                    '   NOSSONUMEROINI BIGINT,' +
                    '   NOSSONUMEROCOR BIGINT,' +
                    '   NOSSONUMEROFIM BIGINT,' +
                    '   VARIACAO       INT,' +
                    '   MODALIDADE     VARCHAR(50), ' + 
                    'FOREIGN KEY(IDBANCO)    REFERENCES bancos(ID), ' +
                    'FOREIGN KEY(IDCARTEIRA) REFERENCES carteiras(ID) );';

        con.query(sql, (err, result, fields) => {
            if (err) return console.log(err)
            console.log('table: solicitacoes');
        })
    }

    /* OPERACOES */ 
    const inserirbancos = (con) => {
        const sql = 'INSERT INTO bancos'+
                    '   (NOME, NUMEROBANCO, LOCALPAGAMENTO, NOMECONTATO, EMAILCONTATO)'+
                    'VALUES'+
                    '   ("Banco do Brasil S.A.", "001", "Págavel em qualque banco.", "Bruno", "dsv1@bremen.com.br"),'+
                    '   ("Banco Santander S.A.", "033", "Págavel em qualque banco.", "Bruno", "dsv1@bremen.com.br"),'+
                    '   ("Caixa Economica Federal", "104", "Págavel em qualque banco.", "Bruno", "dsv1@bremen.com.br"),'+
                    '   ("Banco Bradesco", "237", "Págavel em qualque banco.", "Bruno", "dsv1@bremen.com.br"),'+
                    '   ("Banco Itau", "341", "Págavel em qualque banco.", "Bruno", "dsv1@bremen.com.br");'
       
        con.query(sql, (err, result, fields) => {
            if (err) console.log(err);
            console.log('bancos inseridos com sucesso')
        })

    }

    const insertCarteiras = (con) => {
       
        let bancos
        let carteiras = []        
        
        const bancossql = 'SELECT * FROM bancos;'

        const insertsql = 'INSERT INTO carteiras'+ 
                    ' (DESCRICAO, TIPO, IDBANCO) ' +
                    'VALUES' +
                    ' ?';


        con.query(bancossql, function(err, result, fields) {

            if (err) console.log('Erro ao carregar bancos')                         
            bancos = result                                  

            bancos.forEach(banco => {
                
                carteiras.push( ['Simples   c/ Registro', 'TC_SIMPLES',  banco.ID] )
                carteiras.push( ['Rápida    c/ Registro', 'TC_RAPIDA',   banco.ID] )
                carteiras.push( ['Indexada  c/ Registro', 'TC_INDEXADA', banco.ID] )
                carteiras.push( ['Vinculada c/ Registro ( Caucionada )', 'TC_CAUCIONADA',  banco.ID] )
                carteiras.push( ['Vinculada c/ Registro ( Descontada )', 'TC_DESCONTADA',  banco.ID] )
                carteiras.push( ['Direta    c/ Registro ( Descontada )', 'TC_DIRETA',  banco.ID] )      
                
            })           

                  
            con.query(insertsql, [carteiras], (err, result, fields) => {
                if (err) return console.log(err)
                console.log('Carteiras inseridas com sucesso.')
            })            
        })       
    }


    return { createDatabase, setDatabase, createauth, createbancos, inserirbancos, createcarteiras, createsolicitacoes, insertCarteiras}
}