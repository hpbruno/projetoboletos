const app = require('express')()
const consign = require('consign')

consign()
    .include('./config/passport.js')
    .then('./migrations')  
    .then('./config/db.js')
    .then('./config/middlewares.js')  
    .then('./api')
    .then('./config/routes.js')    
    .into(app)

app.listen(4000, () => { console.log('backend online') }) 