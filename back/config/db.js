const { db } = require('../.env')
const mysql = require('mysql')
const bodyParser = require('body-parser')

// parameters 
const host = db.host
const port = db.port
const user = db.user
const password = db.password
const database = db.database

module.exports = app => {

    //configurando o body parser para pegar POSTS mais tarde
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())

    // connection
    const connection = mysql.createConnection({
        host,
        port,
        user,
        password        
    }) 

    // Migrations
    connection.connect(function(err){
    
        if(err) return console.log(err)
        
        // vamos executar as migrations assim que o banco subir
        console.log('Preparando banco!');
        app.migrations.db0001.createDatabase(connection)
        app.migrations.db0001.setDatabase(connection)
        app.migrations.db0001.createauth(connection)    
        app.migrations.db0001.createbancos(connection)    
        app.migrations.db0001.createcarteiras(connection)  
        app.migrations.db0001.createsolicitacoes(connection)        

        /* INSERTS USADOS PARA TESTE */
        //app.migrations.db0001.inserirbancos(connection)
        //app.migrations.db0001.insertCarteiras(connection)      
    })
    
    return { connection }

} 