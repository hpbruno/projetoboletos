const { authSecret } = require('../.env')
const passport = require('passport') 
const passportJwt = require('passport-jwt')
const { Strategy, ExtractJwt } = passportJwt

module.exports = app => {

    const params = {
        secretOrKey: authSecret, 
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken() // token estraido da requisicao
    }

    const strategy = new Strategy(params, (payload, done) => {
        
        const con = app.config.db.connection
        
        const sql = 'SELECT * FROM _auth WHERE ID = ?'        
        values = [ payload.ID ]

        console.log(payload.ID)

        con.query(sql, [values], (err, result, fields) => {
            if (err) return console.log(err)
            
            let client = result
            client.forEach(cli => {
                
                try {                    
                    done(null, cli ? { ...payload } : false)                    
                } catch(err) {
                    done(err, false)
                }                               
            })        
        })

    })

    passport.use(strategy)

    return { 
        authenticate: () => passport.authenticate('jwt', { session: false })
    }

}