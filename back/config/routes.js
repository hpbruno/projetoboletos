/*
 - @Author: Bruno Henrique
*/
module.exports = app => {

    app.route('/geratoken')
        .post(app.api.auth.gerarAuth)
    
    app.route('/auth')        
        .post(app.api.auth.validate)    
        
    app.route('/validateToken')
        .post(app.api.auth.validateToken)

    app.route('/validation')
        .all(app.config.passport.authenticate())
        .post(app.api.solicitacoes.validarsolicitacao)

    app.route('/bank')
        .all(app.config.passport.authenticate())
        .get(app.api.bancos.retornarBancos)

    app.route('/carteiras')
        .all(app.config.passport.authenticate())
        .post(app.api.bancos.retonarCarteirasBanco)

    app.route('/solicitacoespendentes') 
        .post( app.api.solicitacoes.retornarsolicitacoes )
}